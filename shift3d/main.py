from pathlib import Path
import typing as T
import numpy as np
from PIL import (
    Image,
    ImageColor,
)
import PIL

HERE = Path(__file__).parent.absolute()

EXAMPLE_DIR = HERE.parent / "cubes-on-table"
IMG_PATH = EXAMPLE_DIR / "img.png"
DEPTH_PATH = EXAMPLE_DIR / "depth.png"

OUT_PATH = EXAMPLE_DIR / "shifted.png"
SHIFT_MAP_PATH = EXAMPLE_DIR / "shifted.map.png"

RED = (255, 0, 0, 255)
TRANSPARENT = (0, 0, 0, 0)

def shift_image(img : Image, depth_img : Image):
    
    # Ensure base image has alpha
    img = img.convert("RGBA")
    data = np.array(img)
    
    # Ensure depth image is grayscale (for single value)
    depth_img = depth_img.convert("L")
    depth_data = np.array(depth_img)
    deltas = np.array((depth_data / 255.0) * float(SHIFT_AMOUNT), dtype=int)
    
    shifted_data = np.zeros_like(data)

    width = img.width
    height = img.height

    for y, row in enumerate(deltas):
        for x, dx in enumerate(row):
            x2 = x+dx
            if (x2 >= width) or (x2 < 0):
                continue
            shifted_data[y][x2] = data[y][x]
    
    shifted_image = Image.fromarray(shifted_data)
    
    return shifted_image