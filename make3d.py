# Copyright (c) 2022 Kyle Schouviller (https://github.com/kyle0654) and the InvokeAI Team

import math
from typing import Literal, Optional, get_args

import numpy as np
from PIL import Image, ImageOps, ImageChops
from scipy import ndimage

from invokeai.app.invocations.primitives import ColorField, ImageField, ImageOutput
from invokeai.app.util.misc import SEED_MAX, get_random_seed
from invokeai.backend.image_util.cv2_inpaint import cv2_inpaint
from invokeai.backend.image_util.lama import LaMA
from invokeai.backend.image_util.patchmatch import PatchMatch
from controlnet_aux import (
    MidasDetector,
    ZoeDetector,
)

from invokeai.app.services.image_records.image_records_common import ImageCategory, ResourceOrigin
from .baseinvocation import BaseInvocation, InputField, InvocationContext, invocation
from .image import PIL_RESAMPLING_MAP, PIL_RESAMPLING_MODES
from .controlnet_image_processors import (
    ZoeDepthImageProcessorInvocation,
    MidasDepthImageProcessorInvocation,
    ImageProcessorInvocation,
)


def infill_methods() -> list[str]:
    methods = ["tile", "solid", "lama", "cv2"]
    if PatchMatch.patchmatch_available():
        methods.insert(0, "patchmatch")
    return methods
INFILL_METHODS = Literal[tuple(infill_methods())]
# DEFAULT_INFILL_METHOD = "patchmatch" if "patchmatch" in get_args(INFILL_METHODS) else "tile"

DEPTH_METHODS = Literal[("midas", "zoe",)]

OUTPUT_CHOICES = Literal[("parallel", "stereo", "left_only", "right_only", "red_green", "wiggle")]

@invocation("shift3d", title="Shift Image", tags=["image", "infill", "depth", "3d"], category="inpaint", version="1.0.0")
class ShiftImage(BaseInvocation):
    """Shifts an image by a given amount, producing transparent pixels in place  of unknown information."""

    image : ImageField = InputField(description="Base image")
    depthmap : ImageField = InputField(description="A depth map to use")
    intensity : int = InputField(default=10, description="The intensity of the 3D effect")

    def shift_image(self, img, depth_img, shift_amount=10):
        # Ensure base image has alpha
        img = img.convert("RGBA")
        data = np.array(img)

        # Ensure depth image is grayscale (for single value)
        depth_img = depth_img.convert("L")
        depth_data = np.array(depth_img)
        deltas = np.array((depth_data / 255.0) * float(shift_amount), dtype=int)

        shifted_data = np.zeros_like(data)

        width = img.width
        height = img.height

        for y, row in enumerate(deltas):
            width = len(row)
            x = 0
            while x < width:
                dx = row[x]
                if x+dx >= width:
                    break
                if x-dx < 0:
                    shifted_data[y][max(x-dx,0)] = [0,0,0,0]
                else:
                    shifted_data[y][min(max(x-dx,0), width-1)] = data[y][x]
                # shifted_data[y][x-dx] = data[y][x]
                x += 1

        shifted_image = Image.fromarray(shifted_data)

        # Clean up the "noisy" unknown spaces
        alphas_image = Image.fromarray(
            ndimage.binary_fill_holes(
                ImageChops.invert(
                    shifted_image.getchannel("A")
                )
                )
        ).convert("1")
        shifted_image.putalpha(ImageChops.invert(alphas_image))
        
        return shifted_image

    def invoke(self, context: InvocationContext) -> ImageOutput:
        img = context.services.images.get_pil_image(self.image.image_name)
        depthmap = context.services.images.get_pil_image(self.depthmap.image_name)
        shifted = self.shift_image(img, depthmap, self.intensity)
        image_dto = context.services.images.create(
            image=shifted,
            image_origin=ResourceOrigin.INTERNAL,
            image_category=ImageCategory.CONTROL,
            session_id=context.graph_execution_state_id,
            node_id=self.id,
            is_intermediate=self.is_intermediate,
            workflow=context.workflow,
        )
        processed_image_field = ImageField(image_name=image_dto.image_name)
        return ImageOutput(
            image=processed_image_field,
            # width=processed_image.width,
            width=image_dto.width,
            # height=processed_image.height,
            height=image_dto.height,
            # mode=processed_image.mode,
        )



@invocation("make3dpair", title="Make Pair", tags=["image", "infill", "depth", "3d"], category="inpaint", version="1.0.0")
class MakePair(BaseInvocation):
    """Creates a 3D image from a 2D image. Use 'Shift Image' with an inpainting node for the one of the images."""

    left : ImageField = InputField(description="Left Image")
    right : ImageField = InputField(description="Right Image")

    def pair_images(self, left, right):
        # Ensure base image has alpha
        width = left.width + right.width
        height = max(left.height, right.height)
        mode = "RGBA"
        cross = Image.new(mode, (width, height))
        left.convert(mode)
        right.convert(mode)
        cross.paste(left)
        cross.paste(right, (left.width, 0))
        return cross

    def invoke(self, context: InvocationContext) -> ImageOutput:
        left = context.services.images.get_pil_image(self.left.image_name)
        right= context.services.images.get_pil_image(self.right.image_name)
        paired = self.pair_images(left, right)
        image_dto = context.services.images.create(
            image=paired,
            image_origin=ResourceOrigin.INTERNAL,
            image_category=ImageCategory.CONTROL,
            session_id=context.graph_execution_state_id,
            node_id=self.id,
            is_intermediate=self.is_intermediate,
            workflow=context.workflow,
        )
        processed_image_field = ImageField(image_name=image_dto.image_name)
        return ImageOutput(
            image=processed_image_field,
            # width=processed_image.width,
            width=image_dto.width,
            # height=processed_image.height,
            height=image_dto.height,
            # mode=processed_image.mode,
        )
