# Make 3D

As long as photography has been around, so has stereo photography, an art form that literally
adds depth to images by tricking the brain.

**Make 3D** is a series of plugins for InvokeAi workflow that provides a 3D image as output.

So far, only creating a lef/right pair is supported. If enough people badger me about it I might
develop other output nodes like wigglegrams or red/green.

## Examples

### Parallel view (uncross your eyes)

![Parallel stereo pair; painting of door, cosy, golden colors](example-1.png)

### Cross-Eyed View

![Cross eyed stereo pair; cute puppies staring into the camera](example-2.png)

# Installation

Copy `make3d.py` to `<invoke_ai_directory>/.venv/lib/python<version>/site-packages/invokeai/app/invocations`.

If InvokeAI is currently running, restart the app and reload the browser.

# Setup

In the workflow are now 2 nodes: **Shift 3d** and **Make Pair**

In order to generate a 3D image, you need the following workflow setup (Make3D nodes in red):

![Make 3D diagram](make3d-invokeai-plugin.drawio.png)

## Important notes for the workflow setup:

Basic premise is this:

1. The original image is the first image of the pair.

2. The original image is then passed to a depth processor to create a depth map. This
  depth map will tell `Shift Image` how far to shift the pixels in an image.

3. We must pass in the original image yet again (with the resulting depth map)
  to `Shift Image`. So `Original Image` + `Depth Map` ==> `Shifted Image`.

4. That was the hard part. Now use the resulting shifted image to be the 2nd pair of the
   stereo image.

Also, some issues that might arise:

- For some reason all depth processors with InvokeAI resize rectangular images to the
  minimum size of 512. So for this reason I recommend resizing (upscaling) the
  depth image. Will the quality be lost? Yeah, but these are depth maps, not the actual
  image so I wouldn't worry too much about that.

- This goes without saying, but portrait-oriented images (where width < height)
  are best suited for 3D viewing since it's easier to cross/uncross your eyes.